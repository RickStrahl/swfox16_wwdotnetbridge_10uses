*** Initialization (app startup)
SET PATH TO ".\bin" ADDITIVE
DO MarkdownParser
CLEAR


*** Straight Code
#IF .T.
do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")


loBridge.LoadAssembly("CommonMark.dll")

TEXT TO lcMarkdown NOSHOW
RAW MARKDOWN WITH COMMONMARK - 
This is some sample Markdown text. This text is **bold** and *italic*.

* List Item 1
* List Item 2
* List Item 3

Great it works!

> ### @icon-info-circle Examples are great
> This is a block quote with a header
ENDTEXT

*
lcHtml = loBridge.InvokeStaticMethod("CommonMark.CommonMarkConverter","Convert",lcMarkdown,null)

? lcHtml
_cliptext = lcHtml
RETURN
#ENDIF

*** Using a class
#IF .F.
loParser = CREATEOBJECT("MarkdownParser")

TEXT TO lcMarkdown NOSHOW
This is some sample Markdown text. This text is **bold** and *italic*.

* List Item 1
* List Item 2
* List Item 3

Great it works!

> ### @icon-info-circle Examples are great
> This is a block quote with a header
ENDTEXT


lcHtml = loParser.Parse(lcMarkdown)
? lcHtml
RETURN
#ENDIF



#IF .T.
TEXT TO lcMarkdown NOSHOW
This is some sample Markdown text. This text is **bold** and *italic*.

* List Item 1
* List Item 2
* List Item 3

Great it works!

> ### ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Commons-emblem-legal.svg/45px-Commons-emblem-legal.svg.png) Examples are great
> This is a block quote with a header
ENDTEXT

lcHtml = Markdown(lcMarkdown)
? lcHtml

lcHtml = Markdown(lcMarkdown)
showHtml(lcHtml)

#ENDIF
