# 10 Cool Examples of using wwDotnetBridge

### Session notes, Slides and samples from the Southwest Fox 2016 session.

wwDotnetBridge lets you call just about any .NET code directly from FoxPro and helps overcome most of the limitations of regular .NET COM interop. Using this small library opens up a number of opportunities to interface with .NET functionality that wouldn't otherwise be available. In this talk, you'll see 10 (or more, depending on time) small, ready to use and practical examples that demonstrate how you can integrate with .NET and use many open source libraries that are available for performing a host of useful everyday tasks that would be difficult or impossible to achieve natively in FoxPro.


### Materials:

* [White Paper](https://bitbucket.org/RickStrahl/swfox16_wwdotnetbridge_10uses/raw/236688bf848381e8956cafd645fc19a68b4d67e6/Documents/Strahl_wwdotnetBridge_10Uses.pdf)
* [Session Slides](https://bitbucket.org/RickStrahl/swfox16_wwdotnetbridge_10uses/raw/236688bf848381e8956cafd645fc19a68b4d67e6/Documents/Strahl_wwDotnetBridge_10Uses.pptx)

Related content:

* [wwDotnetBridge White Paper](http://west-wind.com/presentations/wwdotnetbridge/wwDotnetBridge.pdf)
* [wwDotnetBridge Documentation](http://west-wind.com/webconnection/wwClient_docs/?page=_24n1cfw3a.htm)
* [wwDotnetBridge on GitHub](https://github.com/RickStrahl/wwDotnetBridge)