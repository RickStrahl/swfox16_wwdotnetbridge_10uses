LPARAMETERS lcMessage

*** Initialization (app startup)
SET PATH TO ".\bin" ADDITIVE
CLEAR

lcPipename = "MyPipe"

do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")

loBridge.LoadAssembly("wwdotnetbridgedemos.dll")

loServer = loBridge.CreateInstance("wwDotnetBridgeDemos.NamedPipeManager",lcPipename)

IF EMPTY(lcMessage)
   ? loServer.Write("Hello from Pipe Client " + TIME())   
ELSE
	? loServer.Write( lcMessage )
ENDIF

? LoBridge.cErrorMsg

