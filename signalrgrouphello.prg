LPARAMETERS lcMessage, lcGroup

IF EMPTY(lcGroup)
   lcGroup = "Rick"
ENDIF
IF EMPTY(lcMessage)
	lcMessage = "Hello World to group " + lcGroup +  ". Time: " + TIME()
ENDIF

do wwDotNetBridge
loBridge = GetwwDotnetBridge()

loBridge.LoadAssembly("signalrclient.dll")

loProxy = loBridge.CreateInstance("SignalRClient.SignalRClient")
loProxy.Start(null)
loProxy.GroupHello(lcMessage,lcGroup)
loProxy.Stop()

RETURN
