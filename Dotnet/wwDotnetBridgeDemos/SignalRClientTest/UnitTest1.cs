﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SignalRClient;

namespace SignalRClientTest
{
    [TestClass]
    public class SignalRClientTest
    {
        [TestMethod]
        public void CallHelloMethod()
        {
            var handler = new CallbackHandler();

            var proxy = new SignalRClient.SignalRClient();
            proxy.Start(handler);

            // this calls the server
            proxy.Hello("Testing SignalR " + DateTime.Now);

            // call is async so wait for completion
            while(!handler.Done)
                Thread.Sleep(50);
        }

        [TestMethod]
        public void CallHelloGroupMethod()
        {
            var handler = new CallbackHandler();

            var proxy = new SignalRClient.SignalRClient();
            proxy.Start(handler);

            proxy.JoinGroup("Rick");


            // this calls the server
            proxy.GroupHello("Testing SignalR " + DateTime.Now,"Rick");

            // call is async so wait for completion
            while (!handler.Done)
                Thread.Sleep(50);
        }

        [TestMethod]
        public void CallSendPerson()
        {
            var handler = new CallbackHandler();

            var proxy = new SignalRClient.SignalRClient();
            proxy.Start(handler);

            proxy.JoinGroup("Rick");


            var person = new Person
            {
                 Name = "Rick",
                 Company = "West Wind",
                 Email = "rstrahl@west-wind.com",
                 Entered = DateTime.Now
            };

            
            // this calls the server
            proxy.SendPerson(person,"Rick");

            // call is async so wait for completion
            while (!handler.Done)
                Thread.Sleep(50);
        }

    }


    public class CallbackHandler
    {

        public bool Done { get; set; } = false;

        /// <summary>
        /// This receives a callback from the server
        /// </summary>
        /// <param name="message"></param>
        public void Hello(string message)
        {
            Console.WriteLine("Hello result: " + message);
            Done = true;
        }

        public void GroupHello(string message)
        {
            Console.WriteLine("Hello group result: " + message);
            Done = true;
        }

        public void SendPerson(Person person)
        {
            person.Company = "West Wind Technologes " + DateTime.Now;
            Console.WriteLine("SendPerson Callback handled with person: " +
                person.Name + " " + 
                DateTime.Now);          
            Done = true;
        }

        public void WriteMessage(QueueMessageItem message, int elapsed, int waiting)
        {
            Console.WriteLine(message.Message);
        }
        
    }


}
