﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace wwDotnetBridgeDemos
{
    public class SecureFtpClient : IDisposable
    {
        public string Hostname { get; set; }
        public int Port { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string ErrorMessage { get; set; }
        public SftpClient Sftp { get; set; }

        #region High Level Do-Everything Functions

        public SecureFtpClient()
        {
            Port = 22;
        }

        /// <summary>
        /// Call this method before any other operations
        /// </summary>
        /// <returns></returns>
        public bool Connect()
        {
            try
            {
                Sftp = new SftpClient(Hostname, Port, Username, Password);
                Sftp.Connect();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Make sure to call this method to shut down the connection
        /// </summary>
        public void Close()
        {
            if (Sftp != null)
            {
                if (Sftp.IsConnected)
                    Sftp.Disconnect();

                Sftp.Dispose();
                Sftp = null;
            }
        }


        public bool DownloadFileSimple(string remoteFilename, string localFilename,
            string host, int port,
            string username,
            string password)
        {
            try
            {
                using (var sftp = new SftpClient(host, port, username, password))
                {
                    sftp.Connect();

                    File.Delete(localFilename);
                    using (var file = File.OpenWrite(localFilename))
                    {
                        Sftp.DownloadFile(remoteFilename, file);
                    }

                    sftp.Disconnect();
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }

            return true;
        }

        public bool UploadFileSimple(string localFilename, string remoteFilename,
            string host, int port = 22,
            string username = null,
            string password = null)
        {
            try
            {
                using (var sftp = new SftpClient(host, port, username, password))
                {
                    sftp.Connect();

                    using (var file = File.OpenRead(localFilename))
                    {
                        sftp.UploadFile(file, remoteFilename);
                    }

                    sftp.Disconnect();
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }

            return true;
        }





        /// <summary>
        /// High level do everything download function.
        /// Make sure you call Connect() before you call
        /// this function.
        /// </summary>
        /// <param name="remoteFilename"></param>
        /// <param name="localFilename"></param>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool DownloadFile(string remoteFilename, string localFilename)
        {
            try
            {
                using (var file = File.OpenWrite(localFilename))
                {
                    Sftp.DownloadFile(remoteFilename, file);
                }

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
            return true;
        }


        /// <summary>
        /// Upload a file from the local machine to the server
        /// </summary>
        /// <param name="localFilename"></param>
        /// <param name="remoteFilename"></param>
        /// <returns></returns>
        public bool UploadFile(string localFilename, string remoteFilename)
        {
            try
            {
                using (var file = File.OpenRead(localFilename))
                {
                    Sftp.UploadFile(file, remoteFilename);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }

            return true;
        }



        /// <summary>
        /// Get a directory listing of files 
        /// 
        /// returns object list (Name, FullName, Length, LastWriteTime)
        /// 
        /// Make sure to call Connect() before calling this method
        /// </summary>
        /// <param name="filespec"></param>
        /// <returns></returns>
        public SFTPFile[] ListFiles(string filespec)
        {
            if (string.IsNullOrEmpty(filespec))
                filespec = string.Empty;

            try
            {
                var files = Sftp.ListDirectory(filespec)
                    // transform value for easier access in FoxPro
                    // Length is LONG which isn't directly accessible
                    // so we convert it to an int
                    .Select(f => new SFTPFile()
                    {
                        FullName = f.FullName,
                        Name = f.Name,
                        // to int that FoxPro can read
                        Length = Convert.ToInt32(f.Length),
                        LastWriteTime = f.LastWriteTime
                    }).ToArray();
                return files;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// Deletes a file
        /// </summary>
        /// <param name="remoteFilename"></param>
        /// <returns></returns>
        public bool DeleteFile(string remoteFilename)
        {
            try
            {
                Sftp.DeleteFile(remoteFilename);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Changes the remote path 
        /// </summary>
        /// <param name="remotePath"></param>
        /// <returns></returns>
        public bool ChangeDirectory(string remotePath)
        {
            try
            {
                Sftp.ChangeDirectory(remotePath);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }

            return true;
        }

        #endregion


        public void Dispose()
        {
            Close();
        }
    }

    public class SFTPFile
    {
        public string FullName { get; set; }
        public string Name { get; set; }

        public int Length { get; set; }
        public DateTime LastWriteTime { get; set; }
    }
}