CLEAR
do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")

IF !loBridge.LoadAssembly("AlbumServiceProxy.dll")
    ? "Invalid library..." + loBridge.cErrorMsg
    return
ENDIF   

LOCAL loService as AlbumServiceProxy.Proxy.AlbumService
loService = loBridge.CreateInstance("AlbumServiceProxy.ComProxy.AlbumServiceCom")

*** Always check for errors!
IF ISNULL(loService)
   ? "Unable to create service: " + loService.cErrorMsg
   RETURN
ENDIF


loArtist = loService.GetArtist(2)

*** Update a value
loArtist.ArtistName = "Accept " + TRANSFORM(SECONDS())

lnId = loService.SaveArtist(loArtist)
IF lnID < 1
   ? "Failed to update..."
   RETURN
ENDIF


*** Create a new Artist
loArtist = loBridge.CreateInstance("AlbumServiceProxy.ComProxy.Artist")
loArtist.ArtistName = "New Artist " + TRANSFORM(SECONDS())
loArtist.Descriptio = "Description goes here"

lnId = loService.SaveArtist(loArtist)

? "New Id: " + TRANSFORM(lnId)

