do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")

? loBridge.LoadAssembly("TestServiceProxy.dll")


LOCAL loProxy as TestServiceProxy.Proxy.TestService
loProxy = loBridge.CreateInstance("TestServiceProxy.Proxy.TestService")

loArtists = loBridge.InvokeMethod(loProxy,"GetArtists")

? loArtists.Count

FOR lnX = 0 TO loArtists.Count -1
   loArtist = loArtists.Item(lnX)
   ? loArtist.ArtistName 
ENDF

RETURN
