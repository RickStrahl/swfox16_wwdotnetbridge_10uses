﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using TestService;
using Westwind.Utilities;
using Westwind.Utilities.Data;

namespace AlbumViewerServices
{
    /// <summary>
    /// Summary description for AlbumService
    /// </summary>
    [WebService(Namespace = "http://albumviewerservice/albumservice/asmx/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class AlbumService : System.Web.Services.WebService
    {
        private const string CONNECTION_STRING = "AlbumViewer";

        [WebMethod]
        public string HelloWorld(string name = null)
        {
            return "Hello " + name + ". Time is: " + DateTime.Now.ToString("MMM dd, yyyy  hh:mm:ss");
        }

        [WebMethod]
        public Artist[] GetArtists(string artistName)
        {
            if (App.Configuration.ProcessingMode == ProcessingModes.FoxProOleDb)
                return GetArtistsData(artistName);
            else if (App.Configuration.ProcessingMode == ProcessingModes.Dotnet)
                return GetArtistsStatic();

            return null;
        }



        private Artist[] GetArtistsData(string artistName)
        {
            using (var data = new SqlDataAccess(CONNECTION_STRING))
            {
                string sql = "select * from artists ";

                var parms = new List<object>();

                if (!string.IsNullOrEmpty(artistName))
                {
                    sql += "where artistName like ?";
                    parms.Add(artistName + "%");
                }

                List<Artist> artists = data.QueryList<Artist>(sql, parms.ToArray());

                if (artists == null)
                    throw new ApplicationException(data.ErrorMessage);

                return artists.ToArray();
            }
        }


        private Artist[] GetArtistsStatic()
        {
            var artists = new List<Artist>();
            artists.Add(new Artist()
            {
                Id = 0,
                ArtistName = "Foo Fighters",
                Descriptio = "Foo Fighters are an American Band",
                ImageUrl = "http://media.tumblr.com/tumblr_mb76f02FkJ1qfo293.jpg"
            });
            artists.Add(new Artist()
            {
                Id = 1,
                ArtistName = "Motörhead",
                Descriptio = "Motörhead have never JUST been the best rock'n'roll band in the world.",
                ImageUrl = "http://alealerocknroll.com/wp-content/uploads/2014/07/motorhead.jpg"
            });
            artists.Add(new Artist()
            {
                Id = 2,
                ArtistName = "Rollins Band",
                Descriptio = "Raw Power at its sweatiest best.",
                ImageUrl = "http://upload.wikimedia.org/wikipedia/commons/a/a1/Henry_Rollins_2.jpg"
            });

            return artists.ToArray();
        }

        [WebMethod]
        public Artist GetArtist(int id)
        {
            using (var data = new SqlDataAccess(CONNECTION_STRING))
            {
                string sql = "select * from artists where Id = ?";

                var parms = new List<object>();
                parms.Add(id);

                
                Artist artist = data.Find<Artist>(sql, parms.ToArray());

                if (artist == null)
                    throw new ApplicationException(data.ErrorMessage);

                return artist;
            }
        }

        [WebMethod]
        public Album[] GetAlbums()
        {
            if (App.Configuration.ProcessingMode == ProcessingModes.FoxProOleDb)
                return GetAlbumsData();

            return null;
        }


        public Album[] GetAlbumsData()
        {
            using (var data = new SqlDataAccess(CONNECTION_STRING))
            {
                string sql = "select * from albums ";
                var albums = data.QueryList<Album>(sql);

                foreach (var album in albums)
                {
                    album.Artist = data.Find<Artist>(
                        "select * from Artists where id=?", album.ArtistId);

                    album.Tracks = data.Query<Track>(
                        "select * from Tracks where AlbumId=?", album.Id)
                        .ToList();
                }

                return albums.ToArray();
            }
        }

        [WebMethod]
        public Album[] GetAlbumsQuery(AlbumFilterParms filter)
        {
            if (App.Configuration.ProcessingMode == ProcessingModes.FoxProOleDb)
                return GetAlbumsDataQuery(filter);

            return null;
        }

        public Album[] GetAlbumsDataQuery(AlbumFilterParms filter)
        {
            using (var data = new SqlDataAccess(CONNECTION_STRING))
            {
                string sql = "select alb.* " +
                                "      from albums alb, artists as art " +
                                "      where art.id = alb.artistId ";

                var parms = new List<object>();

                if (filter != null)
                {
                    if (!string.IsNullOrEmpty(filter.AlbumName))
                    {
                        sql += "and alb.title like ? ";
                        parms.Add(filter.AlbumName + "%");
                    }

                    if (filter.AlbumYear > 1900)
                    {
                        sql += "and alb.year like ? ";
                        parms.Add(filter.AlbumYear);
                    }

                    if (!string.IsNullOrEmpty(filter.ArtistName))
                    {
                        sql += "and art.ArtistName like ? ";
                        parms.Add(filter.ArtistName + "%");
                    }
                    //if (!string.IsNullOrEmpty(filter.TrackName))
                    //{
                    //    sql += "and trk.SongName like ? ";
                    //    parms.Add(filter.TrackName + "%");
                    //}
                }

                var albums = data.QueryList<Album>(sql, parms.ToArray());

                foreach (var album in albums)
                {
                    album.Artist = data.Find<Artist>(
                        "select * from Artists where id=?", album.ArtistId);

                    album.Tracks = data.Query<Track>(
                        "select * from Tracks where AlbumId=?", album.Id)
                        .ToList();
                }

                return albums.ToArray();
            }
        }

        [WebMethod]
        public Album GetAlbum(string albumName)
        {
            if (App.Configuration.ProcessingMode == ProcessingModes.FoxProOleDb)
                return GetAlbumData(albumName);

            return null;
        }


        public Album GetAlbumData(string albumName)
        {
            using (var data = new SqlDataAccess(CONNECTION_STRING))
            {

                string sql = "select * from albums where title like ?";

                var album = data.Find<Album>(sql, albumName + "%");
                if (album == null)
                    throw new ArgumentException("Album not found: " + data.ErrorMessage);

                album.Artist = data.Find<Artist>("select * from artists where id=?", album.ArtistId);
                album.Tracks = data.QueryList<Track>("select * from tracks where albumid=?", album.Id);

                return album;
            }
        }

        [WebMethod]
        public Complex GetComplexType()
        {
            var complex = new Complex();

            complex.LongNumber = 10L;
            complex.Single = 2.34422F;
            complex.Float = 3.333333F;
            complex.Unsigned64BitInteger = 4443;
            complex.SingleChar = 'f';
            complex.Byte = 10;
            complex.Bytes = new byte[] {48, 49, 50, 51, 52};
            complex.Guid = Guid.NewGuid();
            complex.Color = Colors.Green;
            complex.NullableInt = null;
            complex.SailStruct = new SailStruct()
            {
                 Color = Colors.Orange,
                 Size = 4.2M,
                 MastSize = 370
            };
            complex.Strings = new string[] { "foxpro", "web service", ".net" };

            return complex;
        }

        [WebMethod]
        public int SaveArtist(Artist artist)
        {
            using (var data = new SqlDataAccess(CONNECTION_STRING))
            {

                data.ParameterPrefix = "?";
                data.UsePositionalParameters = true;

                string sql = "select * from artists where Id=? ";
                var artistToUpdate = data.Find<Artist>(sql, artist.Id);
                bool isNew = false;

                if (artistToUpdate == null)
                {
                    artistToUpdate = new Artist();

                    // hack: to generate a new id (need a real key gen routine)
                    artistToUpdate.Id = DataUtils.GetRandomNumber(200, 9999);
                    isNew = true;
                }

                artistToUpdate.ArtistName = artist.ArtistName;
                artistToUpdate.Descriptio = artist.Descriptio;               
                // ...
                int result = 0;
                if (isNew)
                {
                    sql = "insert into  artists (Id,ArtistName, Descriptio) Values (?,?,?)";
                    result = data.ExecuteNonQuery(sql, artistToUpdate.Id, artistToUpdate.ArtistName, artistToUpdate.Descriptio);
                }                
                else
                {
                    sql = "update artists set ArtistName=?, Descriptio=? where Id=?";
                    result = data.ExecuteNonQuery(sql,  artistToUpdate.ArtistName, artistToUpdate.Descriptio, artistToUpdate.Id);
                }
                
                
                if (result == -1)
                    throw new ApplicationException("Unable to save artist: " + data.ErrorMessage);

                return artistToUpdate.Id;
            }

            
        }

        [WebMethod]
        public string PassComplexType(Complex complex)
        {
            var json = JsonConvert.SerializeObject(complex, Formatting.Indented);

            return json;
        }



        [WebMethod]
        public int GenerateError()
        {
            // generate error with standard .NET exception
            throw new ArgumentException("Custom error generated from .NET");

            return 0;
        }


    }
}
