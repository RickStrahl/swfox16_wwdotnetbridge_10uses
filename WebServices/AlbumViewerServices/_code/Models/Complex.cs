﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbumViewerServices
{
    public class Complex
    {
        public long LongNumber { get; set; }
        public char SingleChar { get; set; }
        public byte Byte { get; set;  }
        public byte[] Bytes { get; set; }
        public Single Single { get; set;  }
        public UInt64 Unsigned64BitInteger { get; set; }
        public Colors Color { get; set; }
        public int? NullableInt { get; set; }
        public SailStruct SailStruct { get; set; }
        public Guid Guid { get; set; }
        public float Float { get; set; }

        public string[] Strings { get; set; }
    }

    public struct SailStruct
    {
        public decimal Size { get; set;  }
        public Colors Color { get; set; }
        public int MastSize { get; set;  }
    }

    public enum Colors
    {
        Red,
        Green,
        Blue,
        Black,
        Orange,
        White
    }
}

