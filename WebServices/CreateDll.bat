REM %1   - service URL
REM %2   - namespace 
REM %3   - filename without extension

REM create a .cs file (you can also add this to a project)
wsdl.exe %1 /n:%2 /parameters:"wsdl.parameters.xml" /o:%3.cs

REM compile into a .NET DLL
C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe /target:library %3.cs