CLEAR
do wwDotNetBridge
SET PROCEDURE TO wwEncryption ADDITIVE

* Sample
o = CREATEOBJECT("wwEncryption")


*** Two-way encryption
IF .T.
    lcSecretKey = "ULTRASeekrit"

	? "Encryption with explicit key:"
	lcOriginal = "Test String"
	lcOriginal2 = "Saksdj asdklajs dlaksjd aslkdj alksdjalkd jalskdj asdjasld kalksdjlaksjdkljasd"
	? lcOriginal
	lcEncrypted =  o.Encryptstring(lcOriginal,lcSecretKey)
	? lcEncrypted
	lcDecrypted =  o.Decryptstring(lcEncrypted,lcSecretKey)
	? lcDecrypted
ENDIF

RETURN