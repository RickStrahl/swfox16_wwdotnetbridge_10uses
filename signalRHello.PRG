LPARAMETERS lcMessage

IF EMPTY(lcMessage)
	lcMessage = "Hello Southwest Fox. Time is " + TIME()
ENDIF

do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")

loBridge.LoadAssembly("signalrclient.dll")


loProxy = loBridge.CreateInstance("SignalRClient.SignalRClient")
loProxy.Start(null)
loProxy.Hello(lcMessage)
loProxy.Stop()