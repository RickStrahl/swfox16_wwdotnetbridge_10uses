/// <reference path="jquery.js" />

var tocConfig = {
    html: null,
    timestamp: new Date()
}

function initializeLayout() {
    var $sidebar = $(".sidebar-left");
    var $toggle = $(".sidebar-toggle");
    var $splitter = $(".splitter");

    // modes: none - with sidebar,  1 no sidebar
    var mode = getUrlEncodedKey("mode");
    
    if (mode)
        mode = mode * 1;
    else
        mode = 0;

    if (window.location.href.startsWith("mk:@MSITStore") ||
        window.location.href.startsWith("file://") ||
        mode === 1) {
        hideSidebar();
    } else {
        $.get("tableofcontents.htm", loadTableOfContents);
        
        function toggleSidebar() {            
            var oldTrans = $sidebar.css("transition");
            $sidebar.css("transition", "width 0.5s ease-in-out");
            if ($sidebar.width() < 10) {
                $sidebar.show();
                $sidebar.width(400);
            } else {
                $sidebar.width(0);
            }

            setTimeout(function() { $sidebar.css("transition", oldTrans) }, 700);
        }
        
        // sidebar or hamburger click handler
        $(document.body).on("click",".splitter i,.sidebar-toggle",toggleSidebar);

        $sidebar.resizable({
            handleSelector: ".splitter",
            resizeHeight: false
        });                
    }
}
function loadTableOfContents(html) {    
    var $tocContent = $("<div>" + getBodyFromHtmlDocument(html) + "</div>").find(".toc-content");        
    $("#toc").html($tocContent.html());

    showSidebar();

    // handle AJAX loading of topics        
    $(".toc").on("click", "li a", loadTopicAjax);

    initializeTOC();               
    return false;
}
function loadTopicAjax() {    
    var $a = $(this);
    var href = $a.attr("href");
    
    if (href.startsWith("_")) {
        $.get(href, function (html) {
            var $html = $(html);

            var title = html.extract("<title>", "</title>");
            window.document.title = title;
            var $content = $html.find(".content-pane");
            if ($content.length > 0) {
                html = $content.html();                                
                $(".content-pane").html(html);
                if (window.history.pushState)
                    window.history.pushState({ title: '', URL: href }, "", href);
            } else
                return;

            var $banner = $html.find(".banner");
            if ($banner.length > 0);                    
            $(".banner").html($banner.html());
            
            highlightCode();
        });
        return false;  // don't allow click
    }
    return true;  // pass through click
};
function initializeTOC() {    

    // if running in frames mode link to target frame and change mode
    if (window.parent.frames["wwhelp_right"]) {
        $(".toc li a").each(function() {
            var $a = $(this);
            $a.attr("target", "wwhelp_right");
            var a = $a[0];
            a.href = a.href + "?mode=1";            
        });
        $("ul.toc").css("font-size", "1em");
    }


    expandTopic('index');

    var page = getUrlEncodedKey("page");
    if (page) {
        page = page.replace(/.htm/i, "");
        expandParents(page);
    }
    if (!page) {        
        page = window.location.href.extract("/_", ".htm");        
        if (page)
            expandParents("_" + page);
    }    

    var topic = getUrlEncodedKey("topic");
    if (topic) {
        var id = findIdByTopic();
        if (id) {
            var link = document.getElementById(id);
            var id = link.id;
            expandTopic(id);
            expandParents(id);
        }
    }

    function searchFilterFunc(target) {
        target.each(function() {
            var $a = $(this).find(">a");
            if ($a.length > 0) {
                var url = $a.attr('href');
                if (!url.startsWith("file:") && !url.startsWith("http")) {
                    expandParents(url.replace(/.htm/i, ""), true);
                }
            }
        });
    }

    $("#SearchBox").searchFilter({
            targetSelector: ".toc li",
            charCount: 3,
            onSelected: debounce(searchFilterFunc, 200)
     });    
}

function hideSidebar() {
    var $sidebar = $(".sidebar-left");
    var $toggle = $(".sidebar-toggle");
    var $splitter = $(".splitter");
    $sidebar.hide();
    $toggle.hide();
    $splitter.hide();
}
function showSidebar() {
    var $sidebar = $(".sidebar-left");
    var $toggle = $(".sidebar-toggle");
    var $splitter = $(".splitter");
    $sidebar.show();
    $toggle.show();
    $splitter.show();
}

function SeeAlsoButton() {
    var SeeAlso = document.getElementById("See");
    if (SeeAlso.style.display == "none")
        SeeAlso.style.display = "";
    else
        SeeAlso.style.display = "none";
}

function expandTopic(topicId) {
    var $href = $("#" + topicId);

    var $ul = $href.next();
    $ul.toggle();

    var $button = $href.prev().prev();        

    if ($ul.is(":visible"))
        $button.removeClass("fa-plus-square-o").addClass("fa-minus-square-o");
    else
        $button.removeClass("fa-minus-square-o").addClass("fa-plus-square-o");    
}

function alwaysExpand(topicId) {    
    $("#" + topicId + "_span").show();
    $("#IMG" + topicId).attr("src", "bmp/minus.gif");
}

function expandParents(id, noFocus) {
    if (!id)
        return;

    var $node = $("#" + id.toLowerCase());    
    $node.parents("ul").show();

    if (noFocus)
        return;

    var node = $node[0];
    if (!node)
        return;

    //node.scrollIntoView(true);
    node.focus();
    setTimeout(function() {
        window.scrollX = 0;
    });

}
function findIdByTopic(topic) {
    if (!topic) {
        var query = window.location.search;
        var match = query.search("topic=");
        if (match < 0)
            return null;
        topic = query.substr(match + 6);
        topic = decodeURIComponent(topic);
    }
    var id = null;
    $("a").each(function () {        
        if ($(this).text().toLowerCase() == topic.toLocaleLowerCase()) {
            id = this.id;
            return;
        }
    });
    return id;
}


function tocCollapseAll() {
    
    $("ul.toc > li ul:visible").each(function () {
        var $el = $(this);
        var $href = $el.prev();
        var id = $href[0].id;
        expandTopic(id);
    });
}
function tocExpandAll() {
    $("ul.toc > li ul:not(:visible)").each(function () {
        var $el = $(this);
        var $href = $el.prev();
        var id = $href[0].id;
        expandTopic(id);
    });
}
function mtoParts(address,domain,query) {
    var url = "ma" + "ilto" + ":" + address + "@" + domain;
    if (query)
       url = url + "?" + query;
    return url;
}
